﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Selenium.Core.Main;
using Selenium.Core.Photoalbum;
using Selenium.Core;
using System.Threading;

namespace Selenium.Tests
{
    [TestClass]
    public class Tests
    {
        [TestInitialize]
        public void Init()
        {
            Driver.Initialize();

        }

        [TestMethod]
        public void TestMethod1()
        {

            OurMainPage.GoTo();

            string wixTitle = OurMainPage.GetTitle();
            string properTitle = "Персональный сайт - Главная страница";
            Assert.AreEqual(wixTitle, properTitle);

        }

        [TestMethod]
        public void TestClickOnExelentElement()
        {
            Rating Rating = new Rating();

            PhotoalbumPage.GoTo();
            PhotoalbumPage.VoteForSite(Rating.Отлично);

            Thread.Sleep(10000);
        }

        [TestCleanup]
        public void Quit()
        {
            Driver.Close();
        }
        
   
    }
}
