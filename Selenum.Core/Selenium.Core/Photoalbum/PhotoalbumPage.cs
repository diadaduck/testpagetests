﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;

namespace Selenium.Core.Photoalbum
{
    public static class PhotoalbumPage
    {
        public static void GoTo()
        {
            Driver.Instance.Navigate().GoToUrl("http://testingpesting.ucoz.net/photo/");
        }


        public static String GetTitle()
        {
            string title = Driver.Instance.Title;
            return title;
        }

        public static void ExelentVoteForSite()
        {
            var exelentElement = Driver.Instance.FindElement(By.XPath($"//input[@value='1']"));
            exelentElement.Click();
        }

        //TODO: Прописать функцию, которая в зависимости от value элемента выбирает рейтинг сайта
        public static void VoteForSite(int elementValue)
        {
            var exelentElement = Driver.Instance.FindElement(By.XPath($"//input[@value='{elementValue}']"));
            exelentElement.Click();
        }
    }


    public class Rating
    {
        public readonly int Отлично = 1;
        public readonly int Хорошо = 2;
        public readonly int Неплохо = 3;
        public readonly int Плохо = 4;
        public readonly int Ужасно = 5;
    }

}
