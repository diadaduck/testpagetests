﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;

namespace Selenium.Core
{
    public class Driver
    {
        private static readonly FirefoxOptions options = new FirefoxOptions();

        public static IWebDriver Instance { get; set; }

        public static void Initialize()
        {
             Instance = new FirefoxDriver();
        }


        public static void Close()
        {
            Instance.Quit();

        }
    }
}
