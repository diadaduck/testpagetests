﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Selenium.Core.Main
{
    public static class OurMainPage
    {
        public static void GoTo()
        {
            Driver.Instance.Navigate().GoToUrl("http://testingpesting.ucoz.net/");
        }


        public static String GetTitle()
        {
            string title = Driver.Instance.Title;
            return title;
        }
    }
}
